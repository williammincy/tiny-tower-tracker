var Building = function(options){};
Building.prototype = {
    floors:[],
    bitizens:[],
    totalBitizens:0,
    totalFLoors:0,
    addFloor:function(floor){
        if(this.findFloor(floor)==false){
            this.floors.push(floor);
            return true;
        } else {
            return false;
        }
    },
    removeFloor:function(floor){
        var c = this.findFloor(floor);
        if(c!=false){
            this.floors.splice(c,1);
            return true;
        } else { return false; }
    },
    findFloor:function(floor){
        for(var i=0;i<this.floors.length;i++){
            if(this.floors[i].name==floor.name){
                return this.floors[i];
            }
        }
        return false;
    },
    getImage:function(floor){ return floor.replace(" ","_")+".png";},
    //
    addBitizen:function(bitizen){

    },
    removeBitizen:function(bitizen){

    },
    findBitizen:function(bitizen){
        for(var i=0;i<this.bitizens.length;i++){
            if(bitizen==this.bitizens[i].name){
                return this.bitizens[i];
            }
        }
        return false;
    },
    //
    availableFloors:[
        "Ad Agency",
        "Animation Studio",
        "Aquarium",
        "Arcade",
        "Archery Range",
        "Architect Office",
        "Art Studio",
        "Asian Cuisine",
        "Auto Dealer",
        "BBQ Place",
        "Bakery",
        "Bank",
        "Barber Shop",
        "Bike Shop",
        "Billiard Hall",
        "Book Store",
        "Bowling Alley",
        "Cake Studio",
        "Candle Shop",
        "Casino",
        "Cheese Shop",
        "Chocolatier",
        "Cineplex",
        "Clockmaker",
        "Coffee House",
        "Comedy Club",
        "Comic Store",
        "Courthouse",
        "Cyber Cafe",
        "Day Spa",
        "Dentists Office",
        "Diner",
        "Doctors Office",
        "Donut Shop",
        "Fabric Shop",
        "Fancy Cuisine",
        "Fashion Studio",
        "Film Studio",
        "Fire Station",
        "Floral Studio",
        "Fortune Teller",
        "Frozen Yogurt",
        "Furniture Store",
        "Game Store",
        "Game Studio",
        "Glass Studio",
        "Graphic Design",
        "Grocery Store",
        "Hat Shop",
        "Haunted House",
        "Health Club",
        "Italian Food",
        "Jewelry Store",
        "Karaoke Club",
        "Laboratory",
        "Laundromat",
        "Mapple Store",
        "Martial Arts",
        "Mechanic",
        "Mens Fashion",
        "Mexican Food",
        "Mini Golf",
        "Museum",
        "Music Store",
        "Night Club",
        "Optometrist",
        "Paintball Arena",
        "Pancake House",
        "Park",
        "Pet Shop",
        "Pharmacy",
        "Photo Studio",
        "Pizza Place",
        "Planetarium",
        "Plant Nursery",
        "Pottery Studio",
        "Private Eye",
        "Pub",
        "Racquetball",
        "Record Shop",
        "Recording Studio",
        "Recycling",
        "Rock Climbing",
        "Scoops",
        "Seafood",
        "Security Office",
        "Ship %26 Print",
        "Shoe Store",
        "Sky Burger",
        "Smoothie Shop",
        "Soda Brewery",
        "Software Studio",
        "Stock Exchange",
        "Sub Shop",
        "Surf Shop",
        "Sushi Bar",
        "TV Studio",
        "Tattoo Parlor",
        "Tea House",
        "Tech Store",
        "Theater",
        "Toy Store",
        "Travel Agency",
        "Tutoring Center",
        "Vegan Food",
        "Video Rental",
        "Volleyball Club",
        "Wedding Chapel",
        "Womens Fashion",
        "Wood Shop"
    ]
};