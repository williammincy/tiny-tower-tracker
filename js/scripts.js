$(document).ready(function(e) {
	// extend the global Array object with helper functions
	Array.prototype.indexOf = function ($needle, $haystack, $fromIndex) {
		if ($fromIndex == null) {
			$fromIndex = 0;
		} else if (fromIndex < 0) {
			$fromIndex = Math.max(0, this.length + $fromIndex);
		}
		for (var $i = $fromIndex, $j = $haystack.length; $i < $j; $i++) {
			if ($haystack[$i] === $needle)
			return $i;
		}
		return -1;
	}

	Array.prototype.in_array = function ($needle, $haystack) {
		for(var $i=0; $i<$haystack.length;$i++) {
			if($needle==$haystack[$i]) {
				return $i;	
			}
		}
		return false;
	}
	
	Array.prototype.sortByName = function() {
		var x = this;
		function s(ao,bo){
			var a = ao.name.toLowerCase().replace('.','').replace(' ','').replace(' ','');
			var b = bo.name.toLowerCase().replace('.','').replace(' ','').replace(' ','');
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		}
		return x.sort(s);
	}
	// create models for data
	var Floor = function() {};
	Floor.prototype = {
		number: 0,
		name: "Empty",
		type: "Empty"
	};
	
	var Bitizen = function() {};
	Bitizen.prototype = {
		name: "no name",
		dreamJob: "none",
		floor: 0
	};
	
	var Building = function(){};
	Building.prototype = {
		floors:[],
		bitizens:[],
		totalBitizens:0,
		totalFLoors:0,
		addFloor:function(floor){
			if(this.findFloor(floor)==false){
				this.floors.push(floor);
				return true;
			} else {
				return false;
			}
		},
		removeFloor:function(floor){
			var c = this.findFloor(floor);
			console.log(c);
			if(c!=false){
				console.log(this.floors.splice(c.number-1,1));
				for(var i=c.number-1;i<this.floors.length;i++){
					var u=this.floors[i].number;
					this.floors[i].number--;
					console.log(u+" - "+this.floors[i].number);
				}
				console.log(true);
				return true;
			} else { console.log(false); return false; }
		},
		findFloor:function(floor){
			for(var i=0;i<this.floors.length;i++){
				if(this.floors[i].name==floor.name){
					return this.floors[i];
				}
			}
			return false;
		},
		//
		addBitizen:function(bitizen){
			if(this.findBitizen(bitizen)==false){
				var b = new Bitizen();
				b.name = $('#bitizenName').val();
				b.dreamJob = $('#dreamJob').val();
				b.lives = $('#bitizenFloorLives').val();
				this.bitizens.push(b);
				return true;
			} else {
				return false;
			}
		},
		removeBitizen:function(bitizen){
			var c = this.findBitizen(bitizen);
			if(c!==false){
				this.bitizens.splice(c,1);
				return true;
			} else { return false; }
		},
		findBitizen:function(bitizen){
			for(var i=0;i<this.bitizens.length;i++){
				if(bitizen==this.bitizens[i].name){
					return i;
				}
			}
			return false;
		},
		//
		getImage:function(floor){ return 'img/'+floor.replace(" ","_").replace(" ","_")+".png";},
		// list of all floors available in the game
		availableFloors:[
			"50s Apts",
			"70s Apts",
			"Ad Agency",
			"Animation Studio",
			"Anti Grav Apts",
			"Aquarium",
			"Aquatic Apts",
			"Arcade",
			"Archery Range",
			"Architect Office",
			"Arctic Apts",
			"Art Deco Apts",
			"Art Studio",
			"Asian Cuisine",
			"Auto Dealer",
			"BBQ Place",
			"Bachelor Apts",
			"Bakery",
			"Bank",
			"Barber Shop",
			"Baycrest Apts",
			"Beach Apts",
			"Bike Shop",
			"Billiard Hall",
			"Birchside Apts",
			"Book Store",
			"Bowling Alley",
			"Bridgeview Apts",
			"Brightpoint Apts",
			"Broadleaf Apts",
			"Cake Studio",
			"Candle Shop",
			"Casino",
			"Chateau Apts",
			"Cheese Shop",
			"Chocolatier",
			"Cineplex",
			"Clockmaker",
			"Club Apts",
			"Coffee House",
			"College Apts",
			"Comedy Club",
			"Comic Store",
			"Cottage Apts",
			"Courthouse",
			"Cyber Cafe",
			"Day Spa",
			"Dentists Office",
			"Devonshire Apts",
			"Diner",
			"Doctors Office",
			"Donut Shop",
			"Dover Apts",
			"Eclectic Apts",
			"Egyptian Apts",
			"Estate Apts",
			"Fabric Shop",
			"Fancy Cuisine",
			"Fashion Studio",
			"Fawlty Apts",
			"Film Studio",
			"Fire Station",
			"Floral Studio",
			"Fortune Teller",
			"Frozen Yogurt",
			"Furniture Store",
			"Game Store",
			"Game Studio",
			"Garden Apts",
			"Glass Studio",
			"Glendale Apts",
			"Goldcreek Apts",
			"Gothic Apts",
			"Graphic Design",
			"Greek Apts",
			"Greenwood Apts",
			"Grocery Store",
			"Hacienda Apts",
			"Hat Shop",
			"Haunted House",
			"Health Club",
			"HiFi Apts",
			"High_Tech Apts",
			"Holiday Apts",
            "Home Supply",
			"Honey Apts",
			"Italian Food",
			"Ivymoss Apts",
			"Jewelry Store",
			"Karaoke Club",
			"Laboratory",
            "Laser Tag",
			"Laundromat",
			"Loft Apts",
			"Lotus Apts",
			"Magic Apts",
			"Mapleton Apts",
			"Mapple Store",
			"Martial Arts",
			"Mechanic",
			"Mens Fashion",
			"Mesa Apts",
			"Mexican Food",
			"Millspring Apts",
			"Mini Golf",
			"Mismatch Apts",
			"Mistmont Apts",
			"Moderna Apts",
			"Museum",
			"Music Store",
			"Nerd_Cave Apts",
			"Night Club",
			"Optometrist",
			"Paintball Arena",
			"Pancake House",
			"Park",
			"Party Apts",
			"Pet Shop",
			"Pharmacy",
			"Photo Studio",
			"Pinehurst Apts",
			"Pizza Place",
			"Plainlake Apts",
			"Planetarium",
			"Plant Nursery",
			"Plumbrook Apts",
			"Pottery Studio",
			"Private Eye",
			"Pub",
			"Racquetball",
			"Record Shop",
			"Recording Studio",
			"Recycling",
			"Ridgemill Apts",
			"Rock Climbing",
			"Roman Apts",
			"Rustic Apts",
			"Safari Apts",
			"Santa_Fe Apts",
			"Scoops",
			"Seafood",
			"Security Office",
			"Ship & Print",
			"Ship Apts",
			"Shoe Store",
			"Silverwell Apts",
			"Sky Burger",
			"Smoothie Shop",
			"Soda Brewery",
			"Software Studio",
			"Space Apts",
			"Stock Exchange",
			"Stonecrest Apts",
			"Storage Apts",
			"Sub Shop",
			"Submarine Apts",
			"Surf Shop",
			"Sushi Bar",
			"Sweetside Apts",
			"TV Studio",
			"Tattoo Parlor",
			"Tea House",
			"Tech Store",
			"Theater",
			"Toy Store",
			"Travel Agency",
			"Temple Apts",
			"Tiki Apts",
			"Tuscana Apts",
			"Tutoring Center",
			"Vegan Food",
			"Video Rental",
			"Volleyball Club",
			"Wedding Chapel",
			"Westgrove Apts",
			"Womens Fashion",
			"Wood Shop",
			"Zen Apts"
		]
	};
	// create a building to represent the one in the game
	window.building = new Building();
	// populate floors
	var availableFloors = $("#additionalFloors");
    var living = $("#bitizenFloorLives");
	var dreamJob = $("#dreamJob");
	$.each(window.building.availableFloors,function(index, value) {
        if(value.indexOf("Apt")>-1){
            living.append($('<option>',{
                value : value,
                text : value
            }));
        } else {
			dreamJob.append($('<option>',{
                value : value,
                text : value
            }));
		}
		availableFloors.append($('<option>',{
			value : value,
			text : value
		}));
	});
	// check what's already in local storage
	var indexes = $.jStorage.index();
	if(indexes.length>0){
		$.each(indexes,function(index,value){
			var arr = $.jStorage.get(value);
			switch(value){
				case 'floors':
					window.building.floors = arr;
					var x = arr.sortByName();
					$.each(x,function(index,value){
						$("#additionalFloors option[value='"+value.name+"']").remove();
					});
					$.each(x,function(index, value) {
						$('#availableFloors').append($('<option>',{
							value : value.number,
							text : value.name
						}));
						$('#removableFloors').append($('<option>',{
							value : value.number,
							text : value.name
						}));
					});
					break;
				case 'bitizens':
					window.building.bitizens = arr;
					var x = arr.sortByName();
					$.each(x,function(index,value){
						$("#availableBitizens,#removableBitizen").append($('<option>',{
							value : value.name,
							text : value.name
						}));
					});
					break;
				default:
					break;	
			}
		});
	} else {
		$('#findFloor form').css({display:'none'});
		$('#removeFloor form').css({display:'none'});
		$('#findBitizen form').css({display:'none'});
		$('#removeBitizen form').css({display:'none'});
	}
    // initialize all elements keeping count of things
    updateFloorCount();
    updateBitizenCount();
	// click handlers for all the form buttons
	$("#additionalFloors_btn").click(function(event) {
		event.preventDefault();
		var f = new Floor();
		f.name = $("#additionalFloors").val();
		f.type = "";
		f.number = window.building.floors.length+1;
		//
		window.building.addFloor(f);
		var x = window.building.floors.sortByName();
		$("#removableFloors option,#availableFloors option").remove();
		$.each(x,function(index,value) {
			$("#removableFloors,#availableFloors").append($('<option>',{
					value : value.number,
					text : value.name
				}));
		});
		saveData();
		updateFloorCount();
		$('#findFloor form,#removeFloor form').css({display:'block'});
		$("#additionalFloors option[value='"+f.name+"']").remove();
	});
	$("#availableFloors_btn").click(function(event){
		event.preventDefault();
		var v = window.building.floors[+$("#availableFloors").val()-1];
		$("#findFloorResults").css({display:"block"})
								.html('<strong>Floor '+v.number+'</strong><br/><img src="'+window.building.getImage(v.name)+'" width="100%" alt="'+v.name+'"/>');
	});
	$("#removableFloors_btn").click(function(event){
		event.preventDefault();
		var f = window.building.floors[+$("#removableFloors").val()-1];
		window.building.removeFloor(f);
		$("#removableFloors option,#availableFloors option").remove();
		var x = window.building.floors.sortByName();
		$.each(x,function(index,value) {
			$("#removableFloors,#availableFloors").append($('<option>',{
					value : value.number,
					text : value.name
				}));
		});
		$('#additionalFloors').append($('<option>',{
			value : f.number,
			text : f.name
		}));
		saveData();
		updateFloorCount();
	});
	
	$('#additionalBitizens_btn').click(function(event) {
		event.preventDefault();
		var n = $('#bitizenName').val();
		if(window.building.addBitizen(n)) {
			$('#bitizenName').val("");
			$('#bitizenFloorLives').val(0);
			$("#dreamJob")[0].selectedIndex = 0;
			var x = window.building.bitizens.sortByName();
			$('#availableBitizens option,#removableBitizen option').remove();
			$.each(x,function(index,value){
				$("#availableBitizens,#removableBitizen").append($('<option>',{
					value : value.name,
					text : value.name
				}));
			});
			saveData();
			updateBitizenCount();
		} else {
			var b = window.building.bitizens[window.building.findBitizen(n)];
			alert('This Bitizen already lives here on floor '+b.floor);
		}
	});
	$('#availableBitizens_btn').click(function(event) {
		event.preventDefault();
		var f = $('#availableBitizens').val();
		var c = window.building.findBitizen(f);
		if(c===false){
			alert('Bitizen does not live in this building');
		} else {
			var b = window.building.bitizens[c];
			$('#findBitizenResults')
				.css({display:'block'})
				.html('<strong>Name:</strong> '+b.name+'<br/><strong>Dream Job:</strong> '+b.dreamJob+'<br/><strong>Lives On:</strong> '+b.lives+'<br/>');
		}
	});
	$('#removableBitizen_btn').click(function(event){
		event.preventDefault();
		var c = window.building.findBitizen($('#removableBitizen').val());
		if(c===false){
			alert('Bitizen does not live in this building');
		} else {
			var b = window.building.bitizens[c];
			window.building.removeBitizen($('#removableBitizen').val());
			var x = window.building.bitizens.sortByName();
			$('#availableBitizens option,#removableBitizen option').remove();
			$.each(x,function(index,value){
				$("#availableBitizens,#removableBitizen").append($('<option>',{
					value : value.name,
					text : value.name
				}));
			});
			saveData();
			updateBitizenCount();
		}
	});
	
	
	
	$("#reset_btn").click(function(event) {
		event.preventDefault();
		resetData();
	});
	// setup the main dropdown menu
	$('.dropdown-toggle').dropdown();
	$('#footer-nav li a').click(function(event){
		event.preventDefault();
		checkArticles($(this).attr('data-action'));
	});
	// switches visible section within page based on argument
	function checkArticles(a) {
		// clear search results
		$("#findFloorResults,#findBitizenResults").html("");
		// only show the article for the currently selected action
		$('article').each(function(index,element){
			var d = (($(this).attr('id')==a) ? 'block' : 'none');
			$(this).css('display',d);
		});
	}
	// update count on total floors in building
	function updateFloorCount() {
		var c = window.building.floors.length;
		$('strong.floor, #floorCount').html(c);
		$('.floor-lang').html((c==1) ? 'Floor' : 'Floors');
	}
	// update count on total Bitizens in building
	function updateBitizenCount() {
		var c = window.building.bitizens.length;
		$('strong.bitizen, #bitizenCount').html(c);
		$('.bitizen-lang').html((c==1) ? 'Bitizen' : 'Bitizens');
	}
	// save all data to localStorage
	function saveData(){
		$.jStorage.set('floors',window.building.floors);
		$.jStorage.set('bitizens',window.building.bitizens);
	}
	// delete all data, refresh the page
	function resetData(){
		if(confirm('Are you sure you want to delete all of your data?\nThere is no way to restore it once it has been deleted.')) {
			$.jStorage.deleteKey('floors');
			$.jStorage.deleteKey('bitizens');
			window.location.reload();
		}
	}
});