# Tiny Tower Tracker

**Originally planned on being available in App Stores as 'Tower Tracker'**

This project is an experiment to create an HTML-based mobile application that can be compiled under PhoneGap and made available in the various App Stores.

The application can also be hosted as a website with no code changes.